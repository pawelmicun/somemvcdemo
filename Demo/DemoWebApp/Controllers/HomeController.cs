﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Common.CodeProfiler;
using Common.Datatables;
using DAL.Interfaces;
using Services.ExampleData;
using Services.ExampleData.Model;

namespace DemoWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDbContext _context;
        private readonly IExampleService _service;

        public HomeController(IDbContext context, IExampleService service)
        {
            _context = context;
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            //var firstRecord = _context.Entities().Author.FirstOrDefault();
            //var secondRecord = _context.Entities().Book.FirstOrDefault();
            var thitdRecord = _context.Entities().BookAuthor.FirstOrDefault();

            if (thitdRecord != null)
            {
                var test1 = thitdRecord.Author.FirstName;
                var test2 = thitdRecord.Book.Description;
            }

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            using (CodeProfiler.Profile("Test Profile"))
            {
                Thread.Sleep(500);
            }

            return View();
        }

        public ActionResult DebugMiniprofiler()
        {
            if (Request.Cookies["MiniProfiler"] == null)
            {
                var cookie = new HttpCookie("MiniProfiler", "MiniProfiler");
                Response.AppendCookie(cookie);
                return Content("Enable");
            }
            else
            {
                var cookie = new HttpCookie("MiniProfiler")
                {
                    Expires = DateTime.Now.AddDays(-1),
                    Value = null
                };
                Response.Cookies.Add(cookie);
                return Content("Disable");
            }
        }

        public JsonResult DataHandler(DTParameters param)
        {
            try
            {
                var data = _service.GetData(param.Search.Value, param.SortOrder, param.Start, param.Length);
                var count = _service.GetDataCount(param.Search.Value);

                //result
                var result = new DTResult<AuthorBooksDto>
                {
                    draw = param.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count,
                };
                return Json(result);

            }
            catch (Exception ex)
            {
                return Json(new {error = ex.Message});
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Complex;
using DAL.Models;
using EntityFramework.Functions;

namespace DAL.Context
{
    public static class ModelDefinedFunctions
    {
        [ModelDefinedFunction("FormatName", "DAL.Context",
            @"(CASE 
            WHEN [Author].[Phone] IS NOT NULL
            THEN [Author].[Phone] + N' '
            ELSE N''
        END) + [Author].[FirstName]")]
        public static string FormatName(this Author author)
        {
            return Function.CallNotSupported<string>();
            /*
            return string.Format("{0}{1}",
                author.FirstName == null ? string.Empty : author.FirstName + " ",
                author.LastName);*/
        }
    }
}

﻿CREATE TABLE [dbo].[Book]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[Title] NVARCHAR(255) NOT NULL,
	[Description] NVARCHAR(255) NULL,
	[ISBN] BIGINT NULL,
)

GO

CREATE INDEX [IX_Book_Column] ON [dbo].[Book] ([Title])

GO

CREATE INDEX [IX_Book_Column_1] ON [dbo].[Book] ([Description])

GO

CREATE INDEX [IX_Book_Column_2] ON [dbo].[Book] ([ISBN])

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Functions;

namespace DAL.Context
{
    public static class NiladicFunctions
    {
        [Function(FunctionType.NiladicFunction, "CURRENT_TIMESTAMP")]
        public static DateTime? CurrentTimestamp()
        {
            return Function.CallNotSupported<DateTime?>();
        }

        [Function(FunctionType.NiladicFunction, "CURRENT_USER")]
        public static string CurrentUser()
        {
            return Function.CallNotSupported<string>();
        }

        [Function(FunctionType.NiladicFunction, "SESSION_USER")]
        public static string SessionUser()
        {
            return Function.CallNotSupported<string>();
        }

        [Function(FunctionType.NiladicFunction, "SYSTEM_USER")]
        public static string SystemUser()
        {
            return Function.CallNotSupported<string>();
        }

        [Function(FunctionType.NiladicFunction, "USER")]
        public static string User()
        {
            return Function.CallNotSupported<string>();
        }


    }
}

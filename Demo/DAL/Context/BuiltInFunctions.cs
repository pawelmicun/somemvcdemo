﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Functions;

namespace DAL.Context
{
    public static class BuiltInFunctions
    {
        [Function(FunctionType.BuiltInFunction, "LEFT")]
        public static string Left(this string value, int count)
        {
            return Function.CallNotSupported<string>();
        }

        [Function(FunctionType.BuiltInFunction, "UPPER")]
        public static string Upper(this string value)
        {
            return Function.CallNotSupported<string>();
        }
    }
}

DELETE [dbo].[BookAuthor]
DELETE [dbo].[Author]
DELETE [dbo].[Book]
GO

SET IDENTITY_INSERT [dbo].[Author] ON
GO

DECLARE @AMAX INT = (SELECT ISNULL(MAX(Id), 0) FROM Author)

insert into [dbo].[Author]
( Id, FirstName, LastName, SecurityNumber )
select	@AMAX + T.I, T.FirstName, T.LastName, T.I
from
(
	select	ROW_NUMBER() OVER (ORDER BY T1.Id, T2.Id) as I,
			T1.FirstName,
			T2.LastName
	from	FirstName T1, LastName T2
) as T
where T.I <= 5000000
GO

SET IDENTITY_INSERT [dbo].[Author] OFF
GO

SET IDENTITY_INSERT [dbo].[Book] ON
GO

DECLARE @BMAX INT = (SELECT ISNULL(MAX(Id), 0) FROM Book)

insert into [dbo].[Book]
( Id, Title )
select	T.I + @BMAX, 'Book ' + CAST(T.I as NVARCHAR)
from
(
	select	ROW_NUMBER() OVER (ORDER BY T1.Id, T2.Id, T3.Id) as I
	from	sysobjects T1, sysobjects T2, sysobjects T3
) as T
where T.I <= 5000000
GO

SET IDENTITY_INSERT [dbo].[Book] OFF
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 1
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 2
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 3
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 4
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 5
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 6
where	A.Id % 2 = 0
GO

insert into [dbo].[BookAuthor] (AuthorId, BookId)
select	A.Id, B.Id
from	[dbo].[Author] A
join    [dbo].[Book] B on A.Id = B.Id + 7
where	A.Id % 2 = 0
GO

update Book set ISBN = Id
GO
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Context;
using DAL.Interfaces;

namespace Services.DbContext
{
    public class DbContextService : IDbContext
    {
        private readonly DbEntities _dbEntities;

        public DbContextService()
        {
            _dbEntities = new DbEntities();
        }

        public void Dispose()
        {
            _dbEntities.Dispose();
        }

        public DbEntities Entities()
        {
            return _dbEntities;
        }
    }
}

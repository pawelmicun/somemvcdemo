﻿CREATE FUNCTION [dbo].[ufnGetData]
(
	@PersonID int
)
RETURNS TABLE
AS RETURN 
SELECT	ba.Id as Id,
		AuthorId,
		Firstname,
		Lastname,
		Phone,
		BookId,
		Title,
		[Description]	
FROM	dbo.BookAuthor ba
JOIN	dbo.Book b on b.Id = ba.BookId
JOIN	dbo.Author a on a.Id = ba.AuthorId
WHERE	a.Id <= @PersonID
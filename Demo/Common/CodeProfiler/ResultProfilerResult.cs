﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Profiling;

namespace Common.CodeProfiler
{
    public class ResultProfilerResult : IResultProfiler
    {
        private readonly string _name;
        private readonly Stopwatch _stopwatch;

        private string _result;

        public ResultProfilerResult(string name)
        {
            _name = name;
            if (!string.IsNullOrEmpty(name))
            {
                _stopwatch = new Stopwatch();
                _stopwatch.Start();
            }
        }

        public void Dispose()
        {
        }

        public void Result(string message)
        {
            if (_stopwatch != null)
            {
                _result = message;
                _stopwatch.Stop();

                var profiler = MiniProfiler.Current;
                if (profiler != null && profiler.Head != null)
                {
                    profiler.Head.AddCustomTiming(_name, new CustomTiming(MiniProfiler.Current, message)
                    {
                        Id = Guid.NewGuid(),
                        DurationMilliseconds = _stopwatch.ElapsedMilliseconds,
                        FirstFetchDurationMilliseconds = _stopwatch.ElapsedMilliseconds,
                        ExecuteType = "QUERY"
                    });

                }
            }
        }

        public void Exception(string message)
        {
            if (_stopwatch != null)
            {
                _result = message;
                _stopwatch.Stop();

                var profiler = MiniProfiler.Current;
                if (profiler != null && profiler.Head != null)
                {
                    profiler.Head.AddCustomTiming(_name, new CustomTiming(MiniProfiler.Current, message)
                    {
                        Id = Guid.NewGuid(),
                        DurationMilliseconds = _stopwatch.ElapsedMilliseconds,
                        FirstFetchDurationMilliseconds = _stopwatch.ElapsedMilliseconds,
                        ExecuteType = "ERROR",
                    });

                }
            }
        }
    }
}

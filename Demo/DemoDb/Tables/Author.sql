﻿CREATE TABLE [dbo].[Author]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[FirstName] NVARCHAR(255) NOT NULL,
	[LastName] NVARCHAR(255) NOT NULL,
	[Phone] NVARCHAR(255) NULL,
	[SecurityNumber] BIGINT NULL,
)

GO

CREATE INDEX [IX_Author_Column] ON [dbo].[Author] ([FirstName])

GO

CREATE INDEX [IX_Author_Column_1] ON [dbo].[Author] ([LastName])

GO

CREATE INDEX [IX_Author_Column_2] ON [dbo].[Author] ([Phone])

GO

CREATE INDEX [IX_Author_Column_3] ON [dbo].[Author] ([FirstName], [LastName])

GO

CREATE INDEX [IX_Author_Column_4] ON [dbo].[Author] ([SecurityNumber])

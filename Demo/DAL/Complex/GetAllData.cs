﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Complex
{
    [ComplexType]
    public class GetAllData
    {
        public long Id { get; set; }
        public long AuthorId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public long BookId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

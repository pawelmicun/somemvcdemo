﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Profiling;

namespace Common.CodeProfiler
{
    public class CodeProfiler
    {
        public static IDisposable Profile(string name)
        {
            var profiler = MiniProfiler.Current;

            return profiler != null
                ? new CodeProfilerResult(profiler.Step(name))
                : new CodeProfilerResult(null);
        }

        public static IResultProfiler ResultProfile(string name)
        {
            var profiler = MiniProfiler.Current;

            return profiler != null
                ? new ResultProfilerResult(name)
                : new ResultProfilerResult(null);
        }
    }
}

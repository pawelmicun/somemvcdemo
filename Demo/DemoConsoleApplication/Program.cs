﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using Nest;
using Services;
using Services.ExampleData;
using Services.ExampleData.Model;

namespace DemoConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            ServiceInstaller.InstallServices(builder);

            var container = builder.Build();
            using (var scope = container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IExampleService>();

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);

                settings.MapDefaultTypeIndices(d => d
                    .Add(typeof(AuthorBooksDto), "author-book"));

                var client = new ElasticClient(settings);

                var count = service.GetSQLDataCount();
                var step = 100000;

                for (int i = 0; i < count; i += step)
                {
                    Console.WriteLine("Step {0} from {1} START READ DATA", i, count);
                    var data = service.GetSQLData("AuthorId,BookId,Id", i, step);

                    Console.WriteLine("Step {0} from {1} START INDEX", i, count);
                    client.IndexMany(data);
                }
            }
        }
    }
}

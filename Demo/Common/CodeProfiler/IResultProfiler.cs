﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CodeProfiler
{
    public interface IResultProfiler : IDisposable
    {
        void Result(string message);
        void Exception(string message);
    }
}

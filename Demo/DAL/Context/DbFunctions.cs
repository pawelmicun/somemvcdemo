﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Complex;
using EntityFramework.Functions;

namespace DAL.Models
{
    public partial class EntitiesContext : DbContext
    {
        [Function(FunctionType.TableValuedFunction, "ufnGetData", "DAL.Models.EntitiesContext", Schema = "dbo")]
        public IQueryable<GetAllData> ufnGetData(
            [Parameter(DbType = "int", Name = "PersonID")] int? personId)
        {
            ObjectParameter personIdParameter = personId.HasValue
                ? new ObjectParameter("PersonID", personId)
                : new ObjectParameter("PersonID", typeof(int));

            return this.ObjectContext().CreateQuery<GetAllData>(
                string.Format("[ufnGetData](@PersonID)"), personIdParameter);
        }

        [Function(FunctionType.TableValuedFunction, "ufnSecurity", "DAL.Models.EntitiesContext", Schema = "dbo")]
        public IQueryable<AuthorSecurity> ufnSecurity(
            [Parameter(DbType = "int", Name = "PersonID")] int? personId)
        {
            ObjectParameter personIdParameter = personId.HasValue
                ? new ObjectParameter("PersonID", personId)
                : new ObjectParameter("PersonID", typeof(int));

            return this.ObjectContext().CreateQuery<AuthorSecurity>(
                string.Format("[ufnSecurity](@PersonID)"), personIdParameter);
        }
    }
}

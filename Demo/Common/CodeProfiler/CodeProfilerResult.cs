﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CodeProfiler
{
    internal class CodeProfilerResult : IDisposable
    {
        private readonly IDisposable _step = null;

        public CodeProfilerResult(IDisposable step)
        {
            _step = step;
        }

        public void Dispose()
        {
            if (_step != null)
            {
                _step.Dispose();
            }
        }
    }
}

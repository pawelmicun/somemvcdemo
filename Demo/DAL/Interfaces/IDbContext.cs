﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Context;

namespace DAL.Interfaces
{
    public interface IDbContext : IDisposable
    {
        DbEntities Entities();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DAL.Interfaces;
using Services.DbContext;
using Services.ExampleData;

namespace Services
{
    public static class ServiceInstaller
    {
        public static void InstallServices(ContainerBuilder builder)
        {
            builder.RegisterType<DbContextService>().As<IDbContext>().InstancePerDependency();
            builder.RegisterType<ExampleService>().As<IExampleService>().InstancePerDependency();
        }
    }
}

/// <reference path="typings/jquery/jquery.d.ts"/>
/// <reference path="typings/jquery.dataTables/jquery.dataTables.d.ts"/>
var IndexModule;
(function (IndexModule) {
    function initIndexModule(dataHandler) {
        var _dataHandler = dataHandler;
        var _dataTable = $('#datatab').DataTable({
            "serverSide": true,
            "searchDelay": 500,
            "ajax": {
                "type": "POST",
                "url": _dataHandler,
                "contentType": 'application/json; charset=utf-8',
                'data': function (data) {
                    return data = JSON.stringify(data);
                }
            },
            "processing": true,
            "columns": [
                { "data": "Firstname" },
                { "data": "Lastname" },
                { "data": "SecurityNumber" },
                { "data": "Phone" },
                { "data": "Title" },
                { "data": "Description" },
                { "data": "ISBN" }
            ],
            "order": [0, "asc"]
        });
    }
    IndexModule.initIndexModule = initIndexModule;
})(IndexModule || (IndexModule = {}));
//# sourceMappingURL=IndexModule.js.map
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Common.CodeProfiler;
using DAL.Interfaces;
using Nest;
using Services.ExampleData.Model;

namespace Services.ExampleData
{
    public class ExampleService : IExampleService
    {
        protected readonly IDbContext _context;

        public ExampleService(IDbContext context)
        {
            _context = context;
        }

        public IList<AuthorBooksDto> GetData(string search, string sortOrder, int start, int length)
        {
            using (var profiler = CodeProfiler.ResultProfile("NEST"))
            {
                var client = PrepareNestClient();
                var request = new SearchRequest<AuthorBooksDto>()
                {
                    From = start,
                    Size = length,
                    Sort = PrepareNestSort(sortOrder),
                    Query = PrepareNestQuery(search)
                };

                var response = client.Search<AuthorBooksDto>(request);

                if (response.IsValid)
                {
                    profiler.Result(response.DebugInformation);
                    return response.Documents.ToList();
                }

                profiler.Exception(response.DebugInformation);
                throw new ApplicationException("NEST EXCEPTION", response.OriginalException);
            }
        }

        public long GetDataCount(string search)
        {
            using (var profiler = CodeProfiler.ResultProfile("NEST"))
            {
                var client = PrepareNestClient();
                var request = new CountRequest<AuthorBooksDto>()
                {
                    Query = PrepareNestQuery(search)
                };

                var response = client.Count<AuthorBooksDto>(request);

                if (response.IsValid)
                {
                    profiler.Result(response.DebugInformation);
                    return response.Count;
                }

                profiler.Exception(response.DebugInformation);
                throw new ApplicationException("NEST EXCEPTION", response.OriginalException);
            }

        }

        public IList<AuthorBooksDto> GetSQLData(string sortOrder, int start, int length)
        {
            var query = PrepareSQLFilterQuery(GetSQLQuery(), string.Empty);
            return query.OrderBy(sortOrder).Skip(start).Take(length).ToList();
        }

        public long GetSQLDataCount()
        {
            var query = PrepareSQLFilterQuery(GetSQLQuery(), string.Empty);
            return query.Count();
        }

        public IQueryable<AuthorBooksDto> GetSQLQuery()
        {
            var query =
                from a in _context.Entities().Author

                join ba_query in _context.Entities().BookAuthor on a.Id equals ba_query.AuthorId into ba_q
                from ba in ba_q.DefaultIfEmpty()

                join b_query in _context.Entities().Book on ba.BookId equals b_query.Id into b_q
                from b in b_q.DefaultIfEmpty()


                select new AuthorBooksDto
                {
                    AuthorId = a.Id,
                    Firstname = a.FirstName,
                    Lastname = a.LastName,
                    Phone = a.Phone,
                    SecurityNumber = a.SecurityNumber,

                    Id = ba.Id,

                    BookId = b.Id,
                    Title = b.Title,
                    Description = b.Description,
                    ISBN = b.ISBN
                };

            return query;
        }

        private IQueryable<AuthorBooksDto> PrepareSQLFilterQuery(IQueryable<AuthorBooksDto> query, string search)
        {
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x =>
                    x.Firstname.Contains(search) ||
                    x.Lastname.Contains(search) ||
                    x.Title.Contains(search));
            }
            return query;
        }

        private QueryContainer PrepareNestQuery(string search)
        {
            if (string.IsNullOrEmpty(search))
            {
                return new QueryContainer();
            }

            var rules = new List<QueryContainer>();

            //base rulez
            rules.Add(new WildcardQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Firstname),
                Value = string.Format(@"*{0}*", search)
            });

            rules.Add(new FuzzyQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Firstname),
                Value = search,
                //Boost = 1.0,
                //Fuzziness = Fuzziness.Auto,
                //PrefixLength = 0,
                //MaxExpansions = 100
            });

            rules.Add(new WildcardQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Lastname),
                Value = string.Format(@"*{0}*", search)
            });

            rules.Add(new FuzzyQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Lastname),
                Value = search,
            });

            rules.Add(new WildcardQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Title),
                Value = string.Format(@"*{0}*", search)
            });

            rules.Add(new WildcardQuery
            {
                Field = Infer.Field<AuthorBooksDto>(x => x.Description),
                Value = string.Format(@"*{0}*", search)
            });

            return new BoolQuery
            {
                Should = rules.ToArray(),
                MinimumShouldMatch = 1
            };
        }

        public string GetColumnNameFromSortOrder(string sortOrder)
        {
            var sortOrderData = sortOrder.Split(' ');
            if (sortOrderData.Length >= 1)
            {
                return sortOrderData[0];
            }

            return string.Empty;
        }

        public bool GetSortTypeFromSortOrder(string sortOrder)
        {
            var sortOrderData = sortOrder.Split(' ');
            if (sortOrderData.Length == 2)
            {
                if (sortOrderData[1].ToUpper() == "DESC")
                    return false;
            }

            return true;
        }

        private IList<ISort> PrepareNestSort(string sortOrder)
        {
            var columnName = GetColumnNameFromSortOrder(sortOrder);
            var isAscending = GetSortTypeFromSortOrder(sortOrder);

            var dtoType = typeof (AuthorBooksDto);
            var propertyInfo = dtoType.GetProperty(columnName);

            var field = propertyInfo != null
                ? Infer.Field(propertyInfo)
                : null;

            if (field == null)
            {
                return new List<ISort>
                {
                    new SortField
                    {
                        Field = Infer.Field<AuthorBooksDto>(x => x.AuthorId),
                        Order = SortOrder.Ascending
                    }
                };
            }

            switch (columnName)
            {
                case "Firstname":
                case "Lastname":
                case "Phone":
                case "Title":
                case "Description":
                    field = string.Format("{0}.keyword", columnName.ToLower());
                    break;
            }

            return new List<ISort>
            {
                new SortField
                {
                    Field = field,
                    Order = isAscending
                        ? SortOrder.Ascending
                        : SortOrder.Descending
                }
            };
        }

        private ElasticClient PrepareNestClient()
        {
            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings(node);

            settings.DisableDirectStreaming();

            settings.MapDefaultTypeIndices(d => d
                .Add(typeof(AuthorBooksDto), "author-book"));

            var client = new ElasticClient(settings);
            return client;
        }

    }
}

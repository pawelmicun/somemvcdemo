﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Complex;
using DAL.Models;
using EntityFramework.Functions;

namespace DAL.Context
{
    public partial class DbEntities : EntitiesContext
    {
        public DbEntities()
        {
            Database.SetInitializer<DbEntities>(null);
            ((IObjectContextAdapter) this).ObjectContext.CommandTimeout = 300;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.AddFunctions<EntitiesContext>();
            modelBuilder.AddFunctions(typeof(BuiltInFunctions));
            modelBuilder.AddFunctions(typeof(NiladicFunctions));
            modelBuilder.AddFunctions(typeof(ModelDefinedFunctions));
            modelBuilder.ComplexType<GetAllData>();
        }
    }
}

﻿CREATE FUNCTION [dbo].[ufnSecurity]
(
	@PersonID int
)
RETURNS TABLE
AS RETURN 
SELECT	Id
From	dbo.Author a
WHERE	a.Id = @PersonID
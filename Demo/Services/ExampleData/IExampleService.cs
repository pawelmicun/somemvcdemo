﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.ExampleData.Model;

namespace Services.ExampleData
{
    public interface IExampleService
    {
        IList<AuthorBooksDto> GetData(string search, string sortOrder, int start, int length);
        long GetDataCount(string search);

        IList<AuthorBooksDto> GetSQLData(string sortOrder, int start, int length);
        long GetSQLDataCount();
    }
}

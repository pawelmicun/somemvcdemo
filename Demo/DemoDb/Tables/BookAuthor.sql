﻿CREATE TABLE [dbo].[BookAuthor]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[BookId] BIGINT NOT NULL,
	[AuthorId]  BIGINT NOT NULL, 
    CONSTRAINT [FK_BookAuthor_ToBook] FOREIGN KEY ([BookId]) REFERENCES [Book]([Id]), 
    CONSTRAINT [FK_BookAuthor_ToAuthor] FOREIGN KEY ([AuthorId]) REFERENCES [Author]([Id]),
)

GO

CREATE INDEX [IX_BookAuthor_BookId] ON [dbo].[BookAuthor] ([BookId])

GO

CREATE INDEX [IX_BookAuthor_AuthorId] ON [dbo].[BookAuthor] ([AuthorId])

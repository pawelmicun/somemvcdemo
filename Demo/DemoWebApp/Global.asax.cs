﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Services;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;

namespace DemoWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = RegisterAutofac();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterMiniprofiler();
        }

        protected void Application_BeginRequest()
        {
            if (Request.Cookies["MiniProfiler"] != null)
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }

        private void RegisterMiniprofiler()
        {
            //initialize db
            MiniProfilerEF6.Initialize();
            MiniProfiler.Settings.SqlFormatter =
                new StackExchange.Profiling.SqlFormatters.SqlServerFormatter();
            //initialize views
            var copy = ViewEngines.Engines.ToList();
            ViewEngines.Engines.Clear();
            foreach (var item in copy)
            {
                ViewEngines.Engines.Add(new ProfilingViewEngine(item));
            }

            MiniProfiler.Settings.Results_Authorize = IsUserAllowedToSeeMiniProfilerUI;
        }

        private bool IsUserAllowedToSeeMiniProfilerUI(HttpRequest httpRequest)
        {
            if (httpRequest.Cookies["MiniProfiler"] != null)
            {
                return true;
            }
            return false;
        }

        private IContainer RegisterAutofac()
        {
            var builder = new ContainerBuilder();

            ServiceInstaller.InstallServices(builder);
            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            builder.RegisterFilterProvider();

            var container = builder.Build();
            return container;
        }
    }
}

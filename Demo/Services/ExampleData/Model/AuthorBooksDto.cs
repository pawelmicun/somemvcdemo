﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ExampleData.Model
{
    public class AuthorBooksDto
    {
        public long? Id { get; set; }

        public long AuthorId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public long SecurityNumber { get; set; }

        public string Phone { get; set; }

        public long? BookId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public long? ISBN { get; set; }
    }
}
